import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		
		String artikelEingabe;
		int anzahl;
		double nettopreis;
		
		Scanner myScanner = new Scanner(System.in);

		

		// Benutzereingaben lesen
		
		artikelEingabe = liesString("Was moechten Sie bestellen?");
		
		anzahl = liesInt("Geben Sie die Anzahl ein:");
		
		nettopreis = liesDouble("Geben Sie den Nettopreis ein:");
		

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = myScanner.nextDouble();

		// Verarbeiten

		double nettogesamtpreis = anzahl * nettopreis;
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);

		// Ausgeben

		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikelEingabe, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikelEingabe, anzahl, bruttogesamtpreis, mwst, "%");
	
	}
	
	public static String liesString(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.print(text);
		String artikel = myScanner.next();
		return artikel;
	}
	
	public static int liesInt(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		int anzahl = myScanner.nextInt();
		return anzahl;
	}
	
	public static double liesDouble(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		double nettopreis = myScanner.nextDouble();
		return nettopreis;
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		return nettogesamtpreis;
	}
}