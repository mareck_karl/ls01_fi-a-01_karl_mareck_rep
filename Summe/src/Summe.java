import java.util.Scanner;

public class Summe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner tobi = new Scanner(System.in);
		int eingabe = 0;
		int summe = 0;
		int counter = 0;
		
		System.out.println("Gib 'ne Zahl ein, dann rechne ich von 1 bis zur Zahl zusammen.");
		eingabe = tobi.nextInt();
		
		//kopfgesteuerte Schleife
		while(counter <= eingabe) {
		summe = summe + counter;
		counter ++;
		}
		System.out.println("kopfgesteuert");
		System.out.println(summe);
		
		//fu�gesteuerte Schleife
		summe = 0;
		counter = 0;
		do {
		summe = summe + counter;
		counter = counter + 1;
			
		}while(counter <= eingabe);
		System.out.println("fu�gesteuert");
		System.out.println(summe);
		
		
		//Z�hlschleife
		summe = 0;
		
		for(int i = 1; i <= eingabe; i++) {
		summe = summe + i;	
		}
		System.out.println("Z�hlschleife");
		System.out.println(summe);
		
	}

}
