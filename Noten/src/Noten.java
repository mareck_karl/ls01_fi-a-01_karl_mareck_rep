import java.util.Scanner;

public class Noten {

	public static void main(String[] args) {
	
		//Deklaration/Initialisierung der Variablen
		int zahl1 = 0;
		
		//Deklaration/Initialisierung des Scanners
		Scanner tastatur = new Scanner(System.in);
		
		//Aufforderung und Erkennung der Eingabe
		System.out.println("Gib' mal ne Note ein und ich geb' dir 'ne R�ckmeldung dazu wie gut die ist.");
		zahl1 = tastatur.nextInt();
		
		//

		if(zahl1 == 1) {
		System.out.println("1 steht f�r Sehr gut...aber freu' dich nicht zu dolle, du musstest eine Maschine fragen um das zu erfahren. Sicher, dass Du die verdient hast?");
	}
		else if(zahl1 == 2) {
			System.out.println("2 steht f�r Gut...in deinem Fall wohl f�r Gut genug. Ambition ist nicht dein Ding, oder?");
		}
		
		else if(zahl1 == 3) {
			System.out.println("3 steht f�r Befriedigend...ein Wort, dass dir fremd sein sollte.");
		}
		
		else if(zahl1 == 4) {
			System.out.println("4 steht f�r Ausreichend...wir wollten das Wort entt�uschend vermeiden.");
		}
		
		else if(zahl1 == 5) {
			System.out.println("5 steht f�r Mangelhaft...die zweite H�lfte dieses Wortes steht dir noch bevor.");
		}
		
		else if(zahl1 == 6) {
			System.out.println("6 steht f�r Ungen�gend...ein Wort, dass dir sehr bekannt sein sollte.");
		}
		
		else {
			System.out.println("Schulnoten liegen bei uns im Bereich von 1 bis 6. Versuch's damit.");
		}
	}

}
