package raumschiffe;

import java.util.ArrayList;

/**
 * Diese Klasse erstellt ein neues Objekt der Klasse Raumschiffe
 * 
 * @author Karl Mareck
 *
 */

public class Raumschiffe {

	private String nameSchiff;
	private int energieversorgungZustandProzent;
	private int schildeZustandProzent;
	private int huelleZustandProzent;
	private int lebenserhaltungZustandProzent;
	private int torpedoAnzahl;
	private int reparaturAndroidenAnzahl;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	/**
	 * Konstruktor f�r die Klasse Raumschiffe
	 * 
	 * @param nameSchiff Name des Schiffs als String
	 */
	public Raumschiffe() {
		this.nameSchiff = nameSchiff;
	}

	/**
	 * vollparametriserter Konstruktor der Klasse Raumschiffe
	 * 
	 * @param nameSchiff                      Name des Schiffs als String
	 * @param energieversorgungZustandProzent Energieversorgung des Schiffs als
	 *                                        integer
	 * @param schildeZustandProzent           Schilde des Schiffs als integer
	 * @param huelleZustandProzent            H�lle des Schiffs als integer
	 * @param lebenserhaltungZustandProzent   Lebenserhaltung des Schiffs als
	 *                                        integer
	 * @param torpedoAnzahl                   Anzahl der Torpedos als integer
	 * @param reparaturAndroidenAnzahl        Androidenanzahl als integer
	 */
	public Raumschiffe(String nameSchiff, int energieversorgungZustandProzent, int schildeZustandProzent,
			int huelleZustandProzent, int lebenserhaltungZustandProzent, int torpedoAnzahl,
			int reparaturAndroidenAnzahl) {

		this.nameSchiff = nameSchiff;
		this.energieversorgungZustandProzent = energieversorgungZustandProzent;
		this.schildeZustandProzent = schildeZustandProzent;
		this.huelleZustandProzent = huelleZustandProzent;
		this.lebenserhaltungZustandProzent = lebenserhaltungZustandProzent;
		this.torpedoAnzahl = torpedoAnzahl;
		this.reparaturAndroidenAnzahl = reparaturAndroidenAnzahl;
	}

	public void setnameSchiff(String nameSchiff) {
		this.nameSchiff = nameSchiff;
	}

	public String getnameSchiff() {
		return this.nameSchiff;
	}

	public void setenergieversorgungZustandProzent(int energieversorgungZustandProzent) {
		this.energieversorgungZustandProzent = energieversorgungZustandProzent;
	}

	public int getenergieversorgungZustandProzent() {
		return this.energieversorgungZustandProzent;
	}

	public void setschildeZustandProzent(int schildeZustandProzent) {
		this.schildeZustandProzent = schildeZustandProzent;
	}

	public int getschildeZustandProzent() {
		return this.schildeZustandProzent;
	}

	public void sethuelleZustandProzent(int huelleZustandProzent) {
		this.huelleZustandProzent = huelleZustandProzent;
	}

	public int gethuelleZustandProzent() {
		return this.huelleZustandProzent;
	}

	public void setlebenserhaltungZustandProzent(int lebenserhaltungZustandProzent) {
		this.lebenserhaltungZustandProzent = lebenserhaltungZustandProzent;
	}

	public int getlebenserhaltungZustandProzent() {
		return this.lebenserhaltungZustandProzent;
	}

	public void settorpedoAnzahl(int torpedoAnzahl) {
		this.torpedoAnzahl = torpedoAnzahl;
	}

	public int gettorpedoAnzahl() {
		return this.torpedoAnzahl;
	}

	public void setreparaturAndroidenAnzahl(int reparaturAndroidenAnzahl) {
		this.reparaturAndroidenAnzahl = reparaturAndroidenAnzahl;
	}

	public int getreparaturAndroidenAnzahl() {
		return this.reparaturAndroidenAnzahl;
	}

	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}

	/**
	 * Diese Methode gibt den Gesamtzustand des Schiffs aus, bestehend aus: Name,
	 * Energieversorgung, Schilde, H�lle, Lebenserhaltung, Torpedoanzahl und
	 * Androidenanzahl
	 * @param r Bezug auf das Objekt der Klasse Raumschiffe
	 */
	public void zustandRaumschiff(Raumschiffe r) {
		System.out.println("Zustand des Raumschiffes: " + this.getnameSchiff() + "\nZustand der Energieversorgung: "
				+ this.getenergieversorgungZustandProzent() + "\nZustand der Schilde: "
				+ this.getschildeZustandProzent() + "\nZustand der H�lle: " + this.gethuelleZustandProzent()
				+ "\nZustand der Lebenserhaltungssysteme: " + this.getlebenserhaltungZustandProzent()
				+ "\nAnzahl der Photonentorpedos: " + this.gettorpedoAnzahl() + "\nAnzahl der Reparaturandroiden: "
				+ this.getreparaturAndroidenAnzahl());
	}

	/**
	 * Diese Methode gibt Informationen zur Ladung eines bestimmten Schiffes aus
	 * @param r Bezug auf das Objekt der Klasse Raumschiffe
	 */
	public void ladungsverzeichnisAusgeben(Raumschiffe r) {
		System.out.println("Das Raumschiff " + this.getnameSchiff() + " enth�lt folgende Ladung/en: ");
		for (Ladung tmp : ladungsverzeichnis) {
			System.out.println(tmp.getnameLadung() + " " + tmp.getmenge());
		}
	}

	/**
	 * Diese Methode gibt die Meldung aus, dass ein bestimmtes Schiff getroffen
	 * wurde
	 * 
	 * @param r ist der Bezug auf ein bestimmtes angelegtes Objekt der Klasse
	 *          Raumschiffe
	 */
	private void treffer(Raumschiffe r) {
		System.out.println(this.getnameSchiff() + " hat das Ziel getroffen!");
	}

	/**
	 * Diese Methode feuert einen Torpedo ab, reduziert die Zahl der Torpedos um 1
	 * und falls keine Torpedos �brig sind, gibt sie die Meldung "Click" aus
	 * 
	 * @param r ist der Bezug auf ein bestimmtes angelegtes Objekt der Klasse
	 *          Raumschiffe
	 */
	public void torpedoFeuern(Raumschiffe r) {
		if (gettorpedoAnzahl() == 0)
			nachrichtAnAlle("-=Click=-");
		else {
			settorpedoAnzahl(gettorpedoAnzahl() - 1);
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			treffer(r);
		}
	}

	/**
	 * Diese Methode feuert einen Phaser ab und reduziert die Energieversorgung um
	 * 50%, sollte die Energie unter 50% liegen wird die Nachricht "Click"
	 * ausgegeben
	 * 
	 * @param r ist der Bezug auf ein bestimmtes angelegtes Objekt der Klasse
	 *          Raumschiffe
	 */
	public void phaserFeuern(Raumschiffe r) {
		if (getenergieversorgungZustandProzent() < 50)
			nachrichtAnAlle("-=Click=-");
		else {
			setenergieversorgungZustandProzent(getenergieversorgungZustandProzent() - 50);
			nachrichtAnAlle("Phaserkanone abgeschossen");
			treffer(r);
		}
	}

	/**
	 * Diese Methode wird verwendet um Nachrichten wie in phaserFeuern auszugeben
	 * 
	 * @param nachricht ist die Variable, welche beim Aufruf durch den String
	 *                  ersetzt wird, der angezeigt werden soll
	 */
	public void nachrichtAnAlle(String nachricht) {
		broadcastKommunikator.add(nachricht);
		}
	
	/**
	 * Diese Methode gibt bei Aufruf alle in den Broadcastkommunikator gespeicherten Daten aus
	 * @return gibt den Inhalt des broadcastKommunikator zur�ck und macht ihn anzeigbar
	 */
	public static ArrayList <String> logbuchEintrag() {
		return broadcastKommunikator;
	}
}
