package raumschiffe;

/**
 * Diese Klasse erstellt ein neues Objekt der Klasse Ladung
 * 
 * @author Karl Mareck
 */

public class Ladung {

	private String nameLadung;
	private int menge;

	/**
	 * Konstruktor der Klasse Ladung
	 * 
	 * @param nameLadung der Name des neu angelegten Objekts als String
	 */
	public Ladung() {
		this.nameLadung = nameLadung;
	}

	/**
	 * vollparametrisierter Konstruktor der Klasse Ladung
	 * 
	 * @param nameLadung der Name des neu angelegten Objekts als String
	 * @param menge      die Anzahl des neu angelegten Objekts als integer
	 */
	public Ladung(String nameLadung, int menge) {

		this.nameLadung = nameLadung;
		this.menge = menge;
	}

	public void setnameLadung(String nameLadung) {
		this.nameLadung = nameLadung;
	}

	public String getnameLadung() {
		return this.nameLadung;
	}

	public void setmenge(int menge) {
		this.menge = menge;
	}

	public int getmenge() {
		return this.menge;
	}
}
