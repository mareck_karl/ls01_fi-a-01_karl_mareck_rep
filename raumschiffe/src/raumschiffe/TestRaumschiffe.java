package raumschiffe;

public class TestRaumschiffe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/**
		 * ein neues Objekt der Klasse Raumschiffe mit der Bezeichnung klingonen wird
		 * angelegt, erh�lt einen Namen und Werte f�r Energieversorgung, Schilde, H�lle,
		 * Lebenserhaltung, Torpedos und Reparaturandroiden
		 */
		Raumschiffe klingonen = new Raumschiffe("IKS Hegh'ta", 100, 100, 100, 100, 1, 2);

		/**
		 * es werden zwei Objekte der Klasse Ladung mit den Bezeichnungen klingonen1 und
		 * klingonen2 angelegt, die jeweils einen Namen und eine Zahl als Mengenangabe
		 * erhalten
		 */
		Ladung klingonen1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung klingonen2 = new Ladung("Bat'leth Klingonen Schwert", 200);

		/**
		 * beide Ladungsobjekte werden dem Klingonenschiff zugeordnet
		 */
		klingonen.addLadung(klingonen1);
		klingonen.addLadung(klingonen2);

		/**
		 * ein neues Objekt der Klasse Raumschiffe mit der Bezeichnung romulaner wird
		 * angelegt, erh�lt einen Namen und Werte f�r Energieversorgung, Schilde, H�lle,
		 * Lebenserhaltung, Torpedos und Reparaturandroiden
		 */
		Raumschiffe romulaner = new Raumschiffe("IRW Khazara", 100, 100, 100, 100, 2, 2);

		/**
		 * es werden drei Objekte der Klasse Ladung mit den Bezeichnungen romulaner1,
		 * romulaner2 und romulaner3 angelegt, die jeweils einen Namen und eine Zahl als
		 * Mengenangabe erhalten
		 */
		Ladung romulaner1 = new Ladung("Borg-Schrott", 5);
		Ladung romulaner2 = new Ladung("Rote Materie", 2);
		Ladung romulaner3 = new Ladung("Plasma-Waffe", 50);

		/**
		 * die drei Ladungsobjekte werden dem Romulanerschiff zugeordnet
		 */
		romulaner.addLadung(romulaner1);
		romulaner.addLadung(romulaner2);
		romulaner.addLadung(romulaner3);

		/**
		 * ein neues Objekt der Klasse Raumschiffe mit der Bezeichnung vulkanier wird
		 * angelegt, erh�lt einen Namen und Werte f�r Energieversorgung, Schilde, H�lle,
		 * Lebenserhaltung, Torpedos und Reparaturandroiden
		 */
		Raumschiffe vulkanier = new Raumschiffe("Ni'Var", 80, 80, 50, 100, 0, 5);

		/**
		 * es werden zwei Objekte der Klasse Ladung mit den Bezeichnungen vulkanier1 und
		 * vulkanier2 angelegt, die jeweils einen Namen und eine Zahl als Mengenangabe
		 * erhalten
		 */
		Ladung vulkanier1 = new Ladung("Forschungssonde", 35);
		Ladung vulkanier2 = new Ladung("Photonentorpedo", 3);

		/**
		 * die beiden Ladungsobjekte werden dem Vulkanierschiff zugeordnet
		 */
		vulkanier.addLadung(vulkanier1);
		vulkanier.addLadung(vulkanier2);

		/**
		 * Klingonen feuern einen Photonentorpedo auf Romulaner
		 */
		klingonen.torpedoFeuern(romulaner);

		/**
		 * Romulaner feuern einmal mit Phasern auf Klingonen
		 */
		romulaner.phaserFeuern(klingonen);

		/**
		 * Nachricht der Vulkanier an alle
		 */
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch!");

		/**
		 * Klingonen rufen Zustand ihres Schiffes und ihres Ladungsverzeichnisses ab
		 */
		klingonen.zustandRaumschiff(klingonen);
		klingonen.ladungsverzeichnisAusgeben(klingonen);

		/**
		 * Klingonen versuchen zwei weitere Torpedos auf die Romulaner
		 */
		klingonen.torpedoFeuern(romulaner);
		klingonen.torpedoFeuern(romulaner);

		/**
		 * Klingonen rufen ihren Zustand und ihr Ladungsverzeichnis auf
		 */
		klingonen.zustandRaumschiff(klingonen);
		klingonen.ladungsverzeichnisAusgeben(klingonen);

		/**
		 * Romulaner rufen ihren Zustand und ihr Ladungsverzeichnis auf
		 */
		romulaner.zustandRaumschiff(romulaner);
		romulaner.ladungsverzeichnisAusgeben(romulaner);

		/**
		 * Vulkanier rufen ihren Zustand und ihr Ladungsverzeichnis auf
		 */
		vulkanier.zustandRaumschiff(vulkanier);
		vulkanier.ladungsverzeichnisAusgeben(vulkanier);
	}
}
