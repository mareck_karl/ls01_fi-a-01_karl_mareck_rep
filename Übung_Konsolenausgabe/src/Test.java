
public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Aufgabe 1
/*		
		System.out.printf("%4s \n", "**");
		System.out.printf("* %4s \n", "*");
		System.out.printf("%4s \n \n", "**");
*/		
		//Aufgabe 2
/*		
		System.out.printf("0! %2s %19s %3d \n", "=", "=", 1);
		System.out.printf("1! %2s 1 %17s %3d \n", "=", "=", 1);
		System.out.printf("2! %2s 1 %s 2 %13s %3d \n", "=", "*", "=", 2);
		System.out.printf("3! %2s 1 %s 2 %s 3 %9s %3d \n", "=","*", "*", "=", 6);
		System.out.printf("4! %2s 1 %s 2 %s 3 %s 4 %5s %3d \n", "=","*","*","*","=", 24);
		System.out.printf("5! %2s 1 %s 2 %s 3 %s 4 %s 5 %s %3d \n \n", "=", "*", "*", "*", "*", "=", 120);
*/		
		//Aufgabe 3
		
		System.out.printf("%-11s %s", "Fahrenheit","|");
		System.out.printf("%10s \n", "Celsius");
		System.out.println("- - - - - - - - - - -");
		System.out.printf("-20 %9s", "|");
		System.out.printf("%10.2f \n", -28.89);
		System.out.printf("-10 %9s", "|");
		System.out.printf("%10.2f \n", -23.33);
		System.out.printf("+0 %10s", "|");
		System.out.printf("%10.2f \n", -17.78);
		System.out.printf("+20 %9s", "|");
		System.out.printf("%10.2f \n", -6.67);
		System.out.printf("+30 %9s", "|");
		System.out.printf("%10.2f", -1.11);
		
	}

}
