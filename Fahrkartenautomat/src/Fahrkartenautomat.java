﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);						//Initialisierung, neuer Scanner für Konsoleneingabe
      
       double zuZahlenderBetrag;										//Deklaration der Variablen für Methodenaufrufe							
       double rückgabebetrag;											//Deklaration der Variablen für Methodenaufrufe
       
       zuZahlenderBetrag = fahrkartenBestellungErfassen();				//Aufruf der Methode für die Fahrkartenbestellung
       
       rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);			//Aufruf der Methode für die Fahrkartenbezahlung
       
       fahrkartenAusgeben();											//Aufruf der Methode für die Ausgabe der Fahrkarten

       rückgeldAusgeben(rückgabebetrag);								//Aufruf der Methode für die Ausgabe des Rückgeldes
      

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+	//Ausgabe mit 3 Zeilenumbrüchen
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");		
       
       System.out.println("======================================");
       
       main(args);														//erneuter Aufruf der main-Methode um das Programm nach Abschluss automatisch neuzustarten  
    }
    
    		
    
    	//Methode Bestellung erfassen
    public static double fahrkartenBestellungErfassen() {				//Methodendeklaration
    	
    	System.out.println("Wählen Sie Ihre Wunschfahrkarte für Berlin AB aus: ");
    	fahrkartenOptionen();											//Aufruf der Methode zur Eingabe der gewünschten Fahrkarte
    	Scanner tastatur = new Scanner(System.in);						//Scanner für die Eingabe
    	int wunschTicket = tastatur.nextInt();							//Initialisierung der Variable als Eingabe per Scanner
    	double ticketPreis = 0.0;										//Initialisierung der Variable ticketPreis für folgende switch-cases
    	
    	if(wunschTicket < 1 || wunschTicket > 3) {						//Schleife zur Prüfung der eingegebenen Zahl auf <1 und >3
    		System.out.println("Mehr als drei Optionen ham' wa' nich'! 'Ne Zahl von 1 bis 3, wenn's genehm ist!");	//Ausgabe für den Fall, dass die Eingabe <1 oder >3 ist
			wunschTicket = tastatur.nextInt();							//erneute Variablendeklaration um eine erneute Eingabe nach fehlerhafter Eingabe zu ermöglichen
    		while(wunschTicket < 1 || wunschTicket > 3) {				//Schleife für den Fall, dass die Eingabe eine Zahl von 1 bis 3 ist
    		}
    	}
    	
    	switch(wunschTicket) {											//switch-case um die Ergebnisse einer Eingabe zwischen 1 und 3 festzulegen
    	
    	case 1:															//case für die Eingabe der Zahl 1
    		ticketPreis = 2.90;											//in diesem Fall beträgt der Ticketpreis 2.90 Euro
    		break;														//case Ende
    		
    	case 2:															//case für die Eingabe der Zahl 2
    		ticketPreis = 8.60;											//in diesem Fall beträgt der Ticketpreis 8.60 Euro
    		break;														//case Ende
    		
    	case 3:															//case für die Eingabe der Zahl 3
    		ticketPreis = 23.50;										//in diesem Fall beträgt der Ticketpreis 23.50 Euro
    		break;														//case Ende
    	}
    		
        double zuZahlenderBetrag = ticketPreis * ticketPrüfmenge();		//Berechnung des zu zahlenden Betrags mit Aufruf der Methode ticketPrüfmenge
        return zuZahlenderBetrag;										//Bestellung erfassen gibt die Variable zuZahlenderBetrag zurück
    }
    
    	//Methode zum Anzeigen der Fahrkarten-Optionen					gibt bei Aufruf den im Rumpf aufgeführten Text aus
    public static void fahrkartenOptionen() {							//Methodendeklaration
    	System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
    	System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)");
    	System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
    }
 
    	//Methode zur Prüfung der Ticketanzahl
    public static int ticketPrüfmenge() {								//Methodendeklaration
    	System.out.println("Wie viele sollen's denn sein, Meister?");	//Nachfrage nach der Ticketmenge wird ausgegeben
    	Scanner tastatur = new Scanner(System.in);						//Scanner zur Eingabeerkennung
    	int anzahlTickets = tastatur.nextInt();							//Variablendeklaration als Eingabe per Scanner
    	while((anzahlTickets < 1) || (anzahlTickets > 10)) {			//Schleife falls die Eingabe <1 oder >10 ist
    		System.out.println("Ich kann nur zwischen 1 und 10 Tickets drucken, Meister. Konntest du nicht wissen, hast noch 'ne Chance!");	//Ausgabe in der Schleife
    		anzahlTickets = tastatur.nextInt();							//erneute Eingabe der Ticketanzahl nach der Ausgabe der println
    	}
    	
    	return anzahlTickets;											//die Variable anzahlTickets wird von der Methode ticketPrüfmenge zurückgegeben
    }
    
    	//Methode Fahrkarten bezahlen
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {	//Methodendeklaration mit Übergabe der Variable zuZahlenderBetrag aus der main-Methode
    	Scanner tastatur = new Scanner(System.in); 						//Scanner für die Eingabe der eingeworfenen Münze
    	double eingezahlterGesamtbetrag = 0.0;							//Initialisierung der lokalen Variable eingezahlterGesamtbetrag
        double eingeworfeneMünze;										//Deklaration der lokalen Variable eingeworfeneMünze
         
         while(eingezahlterGesamtbetrag < zuZahlenderBetrag)			//Schleife für die Ausgabe des zu zahlenden Restbetrags
         {
      	   System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));	//Berechnung des zu zahlenden Restbetrags und Ausgabe
      	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");	//Ausgabe der akzeptierten Münzen							
      	   eingeworfeneMünze = tastatur.nextDouble();					//eingeworfeneMünze als Eingabe per Scanner deklariert
           eingezahlterGesamtbetrag += eingeworfeneMünze;				//eingezahlterGesamtbetrag wird um eingeworfeneMünze erhöht
         }
         double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;	//Berechnung des Rückgabebetrages
         return rückgabebetrag;											//Die Methode gibt die Variable rückgabeBetrag zurück
    	
    }
    
    	//Methode zum Ausgeben der Fahrkarten
    public static void fahrkartenAusgeben() {							//Methodendeklaration
    System.out.println("\nFahrschein wird ausgegeben");					//Ausgabe
    warte(250);															//Warteintervall bis zur nächsten Ausgabe
    System.out.println("\n\n");											//Ausgabe einer Leerzeile
    }
    
    	//Methode zum Ausgeben des Rückgeldes
    public static void rückgeldAusgeben(double rückgabebetrag) {		//Methodendeklaration mit Übergabe der Variable rückgabebetrag
    	 if(rückgabebetrag > 0.0)										//Schleife falls der Rückgabebetrag über 0 liegt
         {
      	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro\n", rückgabebetrag);	//Ausgabe mit Anzeige des Rückbetrags
      	   System.out.println("wird in folgenden Münzen ausgezahlt:");					
      	   
             while(rückgabebetrag >= 2.0) // 2 EURO-Münzen				//Schleifen für die Münzrückgabe
             {													
          	  muenzeAusgeben(2, "Euro");								//Aufruf der Methode für die Formatierung der Ausgabe
  	          rückgabebetrag -= 2.00;									//rückgabebetrag wird um 2 reduziert
             }													
             while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
             {
          	  muenzeAusgeben(1, "Euro");								//Aufruf der Methode für die Formatierung der Ausgabe
  	          rückgabebetrag -= 1.0;									//rückgabebetrag wird um 1 reduziert
             }
             while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
             {
          	  muenzeAusgeben(50, "Cent");								//Aufruf der Methode für die Formatierung der Ausgabe
  	          rückgabebetrag -= 0.50;									//rückgabebetrag wird um 0.50 reduziert
             }
             while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
             {
            	 muenzeAusgeben(20, "Cent");							//Aufruf der Methode für die Formatierung der Ausgabe
   	          rückgabebetrag -= 0.20;									//rückgabebetrag wird um 0.20 reduziert
             }
             while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
             {
            	 muenzeAusgeben(10, "Cent");							//Aufruf der Methode für die Formatierung der Ausgabe
  	          rückgabebetrag -= 0.10;									//rückgabebetrag wird um 0.10 reduziert
             }
             while(rückgabebetrag >= 0.04)// 5 CENT-Münzen
             {
            	 muenzeAusgeben(5, "Cent");								//Aufruf der Methode für die Formatierung der Ausgabe
   	          rückgabebetrag -= 0.05;									//rückgabebetrag wird um 0.05 reduziert
             }
         }
    }
    
    public static void warte(int millisekunde) {						//Methodendeklaration mit Übergabe der Variable millisekunde
    	for (int i = 0; i < 8; i++)										
        {
           System.out.print("=");
           try {
    			Thread.sleep(millisekunde);
    		} catch (InterruptedException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        }
    }
    
    public static void muenzeAusgeben(int betrag, String einheit) {		//Methodendeklaration mit Übergabe der Variablen betrag und einheit
    	System.out.println(betrag + " " + einheit);						//Ausgabe von betrag und einheit mit einem Leerzeichen dazwischen
    }
}