//AB Schleifen Aufgabe 1: Z�hlen
import java.util.Scanner;

public class ab_Schleifen1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner tobi = new Scanner(System.in);
		int eingabe = 0;

		//hochz�hlen
		System.out.println("Gib 'ne Zahl ein und ich z�hl dir bis dahin hoch.");
		eingabe = tobi.nextInt();

		for(int i = 1; i <= eingabe; i++) {
			System.out.println(i);
		}
		
		//runterz�hlen
		System.out.println("Und r�ckw�rts sieht das Ganze so aus:");
		
		for(int i = eingabe; i >= 1; i--) {
			System.out.println(i);
		}

	}

}
