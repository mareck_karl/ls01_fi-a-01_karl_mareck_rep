import java.util.Scanner;
public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      
	  // Deklaration und Initialisierung der Variablen
	  double zahl1 = 0.0;
      double zahl2 = 0.0;
      double zahl3 = 0.0;
      double ergebnis2, ergebnis3;
      Scanner myScanner = new Scanner(System.in);
      
      programmHinweis();     
      System.out.println("Geben sie die erste Zahl ein");
      zahl1 = myScanner.nextDouble();
      
      System.out.println("Geben sie die zweite Zahl ein");
      zahl2 = myScanner.nextDouble();
      
      System.out.println("Geben sie die dritte Zahl ein");
      zahl3 = myScanner.nextDouble();
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      ergebnis2 = mittelwertberechnung(zahl1, zahl2); //Aufruf der Methode und �bergabe der Werte
      ergebnis3 = mittelwertberechnung(zahl1, zahl2, zahl3);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", zahl1, zahl2, ergebnis2);
      
      System.out.printf("Der Mittelwert von %.2f und %.2f und %.2f ist %.2f\n", zahl1, zahl2, zahl3, ergebnis3);
      
      myScanner.close();
   }
   //R�ckgabe - Datentyp - Methodenname - Parameterliste
   	static double mittelwertberechnung(double zahl1, double zahl2) { //Signatur der Methode/Methodenkopf
	   
	   double ergebnisMethode = 0.0;			//Methodenrumpf
	   ergebnisMethode = (zahl1 + zahl2) / 2.0; //Methodenrumpf
	   return ergebnisMethode;					//Methodenrumpf
   }

   	static double mittelwertberechnung(double zahl1, double zahl2, double zahl3) {
 	   
 	   double ergebnisMethode = 0.0;
 	   ergebnisMethode = (zahl1 + zahl2 + zahl3) / 3.0;
 	   return ergebnisMethode;
    }

   	static void programmHinweis() {
   	System.out.println("Dieses Programm ermittelt den Mittelwert von zwei oder drei Zahlen.");
   	}
}
