//AB Schleifen Aufgabe 4: Folgen
package ab_Schleifen1_Folgen;

public class Folgen {

	public static void main(String[] args) {
				// TODO Auto-generated method stub
		
		//a
		System.out.println("Heute wird gez�hlt:");
		System.out.println("Zuerst r�ckw�rts in 3er Schritten. Wir fangen bei 99 an");
		
		for(int i = 99; i >= 9; i-=3) {
			if(i == 9)
				System.out.println(i);
			
			else
				System.out.print(i + ", ");
		}
		
		//b
		System.out.println("Und nun die Quadratzahlen bis 20!");
	
		for(int i = 1; i <= 20; i++) {
			if(i == 20)
				System.out.println(i*i);
			else
				System.out.print(i*i + ", ");
		}
		
		//c
		System.out.println("Als Drittes folgt eine Z�hlung in 4er Schritten, angefangen bei 2.");
		
		for(int i = 2; i <= 102; i+=4) {
			if(i == 102)
				System.out.println(i);
			else
				System.out.print(i + ", ");
		}
		
		//d
		//System.out.println("Im Vierten Schritt beginnen wir bei 4, z�hlen 12 dazu und addieren mit jeder weiteren Addition 8 auf den Z�hlschritt");
		//int counter = 12;
		
		//for(int i = 4; i <= 1024; counter += 8) {
			//if(i == 4)
				//System.out.print(i + counter);
			//else
				//System.out.print(i + counter + ", ");
		//}
				
		//e
		System.out.println("Und zum Abschluss die 2er Potenzen von der 2. bis zur 16.");
		
		for(int i = 2; i <= 32768; i*=2) {
			if(i == 32768)
				System.out.println(i);
			else
				System.out.print(i + ", ");
		}
	}
}


